import React from 'react';
import { connect } from 'react-redux';
import { pokemonList } from '../actions';
import PokemonCard from '../components/PokemonCard';
import TextField from '@material-ui/core/TextField';
import { withStyles } from '@material-ui/core/styles';

const useStyles = theme => ({
  search: {
    display: 'flex',
    margin: '10px',
    width: '60%'
  },
  list: { display: 'flex', flexWrap: 'wrap' }
});

class Pokemons extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      search: null
    };
  }

  componentWillMount = () => {
    this.props.pokemonList();
  };

  onChangeHandler = event => {
    this.setState({
      search: event.target.value
    });
  };

  renderPokemonList = () => {
    const { search } = this.state;
    let { pokemons } = this.props;
    if (search) {
      const regex = new RegExp(`^${search}`, 'i');
      pokemons = pokemons.filter(({ name }) => name.match(regex));
    }

    return pokemons.map((pokemon, index) => {
      return <PokemonCard key={index} pokemon={pokemon}></PokemonCard>;
    });
  };

  render = () => {
    const { classes } = this.props;

    return (
      <div>
        <h1>List</h1>
        <TextField
          className={classes.search}
          id="standard-basic"
          label="Search"
          onChange={this.onChangeHandler}
        />

        <div className={classes.list}>
          {this.props.pokemons && this.renderPokemonList()}
        </div>
      </div>
    );
  };
}

const mapStateToProps = state => ({
  pokemons: state.pokemon.pokemons
});

const mapDispatchToProps = {
  pokemonList
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(withStyles(useStyles)(Pokemons));
