import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import { Pokemons } from './pages';
import MiniDrawer from './components/Layout';
function App() {
  return (
    <div className="App">
      <Router>
        <MiniDrawer>
          <Switch>
            <Route exact path="/" component={Pokemons} />
          </Switch>
        </MiniDrawer>
      </Router>
    </div>
  );
}

export default App;
